import 'package:mobx/mobx.dart';
part 'item_model.g.dart';

class ItemModel = _ItemModelBase with _$ItemModel;

abstract class _ItemModelBase with Store {
  _ItemModelBase({this.id, this.title, this.completed = false});

  @observable
  String id;

  @observable
  String title;

  @observable
  bool completed;

  @action
  setId(String value) => id = value;

  @action
  setTitle(String value) => title = value;

  @action
  setCheck(bool value) => completed = value;
}
