import 'dart:convert';

List<Tarefa> tarefaFromJson(String str) =>
    List<Tarefa>.from(json.decode(str).map((x) => Tarefa.fromJson(x)));

String tarefaToJson(List<Tarefa> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Tarefa {
  String id;
  String title;
  bool completed;

  Tarefa({
    this.id,
    this.title,
    this.completed,
  });

  factory Tarefa.fromJson(Map<String, dynamic> json) => Tarefa(
        id: json["id"],
        title: json["title"],
        completed: json["completed"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "completed": completed,
      };
}
