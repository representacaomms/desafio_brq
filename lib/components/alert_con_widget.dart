import 'package:desafio_brq/views/tarefas_widget.dart';
import 'package:flutter/material.dart';

Future<void> alertConWidget(context, name) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('OPS! Erro de Conexão'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                  'Você está sem conexao com a internet.\nVocê poderá ver as tarefas salvas no seu dispositivo!')
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Sair'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              var route = new MaterialPageRoute(
                builder: (BuildContext context) =>
                    new TarefasWidget(name: name),
              );
              Navigator.of(context).push(route);
            },
          ),
        ],
      );
    },
  );
}
