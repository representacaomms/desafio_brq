import 'package:desafio_brq/controllers/controller.dart';
import 'package:flutter/material.dart';

Future<void> alertWidget(context, index, item) async {
  var controller = Controller();
  var nameTextField = new TextEditingController(text: item["title"]);
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Editar Tarefa!'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              TextField(
                controller: nameTextField,
                onChanged: (String value) {
                  item["title"] = value;
                },
                //autofocus: true,
                keyboardType: TextInputType.text,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
                decoration: InputDecoration(
                  hasFloatingPlaceholder: true,
                  border: InputBorder.none,
                  labelStyle: TextStyle(
                    color: Colors.black45,
                    fontWeight: FontWeight.w400,
                    fontSize: 25,
                  ),
                ),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Sair'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text('Atualizar'),
            onPressed: () {
              controller.editTitle(nameTextField.text, index);
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
