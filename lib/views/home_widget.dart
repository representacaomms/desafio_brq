import 'package:desafio_brq/components/alert_con_widget.dart';
import 'package:desafio_brq/controllers/controller.dart';
import 'package:desafio_brq/views/tarefas_widget.dart';
import 'package:flutter/material.dart';

class HomeWidget extends StatefulWidget {
  HomeWidget({Key key}) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  var nameTextField = new TextEditingController(text: '');
  var controller = Controller();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: ListView(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      'Bem vindo ao',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 30.0, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'To-doo',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 30.0, fontWeight: FontWeight.bold),
                    ),
                    Container(
                      height: 5,
                      width: 90,
                      color: Colors.blue,
                    )
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 50),
            height: MediaQuery.of(context).size.height / 4,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 5,
                  height: 40,
                  color: Colors.blue,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: TextField(
                    controller: nameTextField,
                    //autofocus: true,
                    keyboardType: TextInputType.text,
                    style: TextStyle(
                      color: Colors.blue,
                      fontSize: 20,
                    ),
                    decoration: InputDecoration(
                      hasFloatingPlaceholder: true,
                      border: InputBorder.none,
                      labelText: "Informe seu nome",
                      labelStyle: TextStyle(
                        color: Colors.black45,
                        fontWeight: FontWeight.w400,
                        fontSize: 25,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height / 3,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: MediaQuery.of(context).size.width - 20,
                child: RaisedButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(
                      18.0,
                    ),
                  ),
                  onPressed: () async {
                    var route = new MaterialPageRoute(
                      builder: (BuildContext context) =>
                          new TarefasWidget(name: nameTextField.text),
                    );

                    var conexao = await controller.connection();
                    conexao == false
                        ? alertConWidget(context, nameTextField.text)
                        : Navigator.of(context).push(route);
                  },
                  child: const Text('Continuar',
                      style: TextStyle(
                        fontSize: 20,
                      )),
                  color: Colors.black,
                  textColor: Colors.white,
                  elevation: 5,
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
