import 'package:desafio_brq/components/alert_widget.dart';
import 'package:desafio_brq/controllers/controller.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter/material.dart';

class TarefasWidget extends StatefulWidget {
  final String name;
  TarefasWidget({Key key, @required this.name}) : super(key: key);

  @override
  _TarefasWidgetState createState() => _TarefasWidgetState();
}

class _TarefasWidgetState extends State<TarefasWidget> {
  var controller = Controller();
  var nameTextField = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 2.5,
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.exit_to_app,
                      color: Colors.blue,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      "Olá ${widget.name},",
                      style: TextStyle(fontSize: 25.0, color: Colors.black),
                    )
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Essa é sua lista de tarefas.',
                      style: TextStyle(fontSize: 18.0, color: Colors.black),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                  width: 80.0,
                  height: 4.0,
                  color: Colors.lightBlue,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(child: Observer(
                  builder: (_) {
                    return Text(
                      '${controller.totalChecked}/${controller.totalList} completos',
                      style: TextStyle(fontSize: 14),
                    );
                  },
                )),
                SizedBox(
                  width: 80,
                ),
                Expanded(
                    child: RaisedButton(
                  color: Colors.white,
                  textColor: Colors.black,
                  splashColor: Colors.white,
                  onPressed: () {
                    for (var i = 0; i < controller.listItens.length; i++) {
                      if (controller.listItens[i]['completed'] == false) {
                        controller.listItens[i]['completed'] = true;
                        var item = controller.listItens[i];

                        controller.editItem(true, item);
                      }
                    }
                  },
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                      side: BorderSide(width: 1.0, color: Colors.black)),
                  child: Text('Completar todos'),
                )),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: IconButton(
                    icon: Icon(Icons.add_circle_outline),
                    tooltip: 'Clique para adicionar',
                    iconSize: 30.0,
                    onPressed: () {
                      controller.addItem(nameTextField.text);
                      nameTextField.text = '';
                    },
                  ),
                ),
                Expanded(
                  child: TextField(
                    controller: nameTextField,
                    decoration: InputDecoration(
                        labelText: "Escreva o que deseja fazer",
                        labelStyle: TextStyle(
                          color: Colors.black45,
                        )),
                  ),
                ),
              ],
            ),
          ),
          Expanded(child: Observer(
            builder: (_) {
              if (controller.listItens.length == 0)
                return Center(
                  child: CircularProgressIndicator(),
                );
              return ListView.builder(
                padding: EdgeInsets.only(top: 10.0),
                itemCount: controller.listItens.length,
                itemBuilder: (_, index) {
                  return Observer(
                    builder: (_) {
                      return ListTile(
                        onTap: () {
                          var item = controller.listItens[index];
                          alertWidget(context, index, item);
                        },
                        title: controller.listItens[index]["completed"] == true
                            ? Text(
                                controller.listItens[index]["title"],
                                style: TextStyle(
                                  decoration: TextDecoration.lineThrough,
                                ),
                              )
                            : Text(controller.listItens[index]['title']),
                        leading: Checkbox(
                          value: controller.listItens[index]["completed"],
                          onChanged: (bool value) {
                            var item = controller.listItens[index];
                            controller.editItem(value, item);
                          },
                        ),
                        trailing: IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () {
                            var item = controller.listItens[index];
                            controller.removeItem(item, index);
                          },
                        ),
                      );
                    },
                  );
                },
              );
            },
          )),
        ],
      ),
    );
  }
}
