import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:mobx/mobx.dart';
import 'package:connectivity/connectivity.dart';
import 'package:path_provider/path_provider.dart';
part 'controller.g.dart';

class Controller = _ControllerBase with _$Controller;

var dio = Dio(
  BaseOptions(
      baseUrl:
          "https://api.fake.rest/895c3a2e-6e22-46b2-b692-80caf408f8dc/todos"),
);

var connectivityResult;

abstract class _ControllerBase with Store {
  @observable
  ObservableList listItens = [].asObservable();

  _ControllerBase() {
    connection();
  }

  @action
  connection() async {
    connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      getAllAPI();
      return true;
    } else {
      _readData().then((data) {
        var items = json.decode(data);
        for (var item in items) {
          listItens.add(item);
        }
      });
      listItens = listItens;
      return false;
    }
  }

  @computed
  int get totalChecked => listItens.where((item) => item['completed']).length;

  @computed
  int get totalList => listItens.length;

  @action
  addItem(dados) async {
    String data = '{"title": "$dados", "completed": false}';
    listItens.add(json.decode(data));
    if (connectivityResult == ConnectivityResult.none) {
      _saveData();
    } else {
      _saveData();
      await postAPI(dados);
    }
  }

  @action
  removeItem(item, index) async {
    listItens.removeAt(index);
    listItens = listItens;
    if (connectivityResult == ConnectivityResult.none) {
      _saveData();
    } else {
      _saveData();

      await deleteAPI(item['id']);
    }
  }

  @action
  editTitle(item, index) {
    listItens[index]['title'] = item;
    if (connectivityResult == ConnectivityResult.none) {
      _saveData();
    } else {
      _saveData();
      putAPI(listItens[index]);
    }
  }

  @action
  editItem(value, item) {
    for (var it in listItens) {
      if (it['id'] == item['id']) {
        it = item;
        it['completed'] = value;
      }
    }
    listItens = listItens;
    if (connectivityResult == ConnectivityResult.none) {
      _saveData();
    } else {
      _saveData();
      putAPI(item);
    }
  }

  @action
  getAllAPI() async {
    final response = await dio.get('');

    if (response.statusCode == 200) {
      var resp = response.data;
      for (var i = 0; i < resp.length; i++) {
        addListOffLine(resp[i]);
      }
    }
  }

  postAPI(title) async {
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      var customHeaders = {'content-type': 'application/json'};
      options.headers.addAll(customHeaders);
      return options;
    }));

    await dio.post("", data: {"title": title, "completed": false});
  }

  deleteAPI(id) async {
    await dio.delete('/$id');
  }

  @action
  putAPI(model) async {
    print(model);
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      var customHeaders = {'content-type': 'application/json'};
      options.headers.addAll(customHeaders);
      return options;
    }));
    var response = await dio.put("/${model['id']}", data: {
      "id": model['id'],
      "title": model['title'],
      "completed": model['completed']
    });
  }

  @action
  addListOffLine(item) {
    Map<String, dynamic> dados = Map();
    dados["completed"] = item["completed"];
    dados["title"] = item["title"];
    dados["id"] = item["id"];
    listItens.add(dados);
    _saveData();
  }

  Future<File> _getFile() async {
    final directory = await getApplicationDocumentsDirectory();
    return File("${directory.path}/data.json");
  }

  @action
  Future<File> _saveData() async {
    String data = json.encode(listItens);
    final file = await _getFile();
    return file.writeAsString(data);
  }

  @action
  Future<String> _readData() async {
    try {
      final file = await _getFile();
      return file.readAsString();
    } catch (e) {
      return null;
    }
  }
}
